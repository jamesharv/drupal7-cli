FROM jamesharv/drupal7:latest

RUN curl -sSL -O https://get.docker.com/builds/Linux/x86_64/docker-1.8.1 && chmod +x docker-1.8.1 && sudo mv docker-1.8.1 /usr/local/bin/docker
RUN curl -L https://github.com/docker/compose/releases/download/1.5.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose && \
    chmod +x /usr/local/bin/docker-compose

RUN apt-get -y update && apt-get -y install \
      ack-grep \
      build-essential \
      git-core \
      libcurl4-openssl-dev \
      libffi-dev
      libreadline-dev \
      libsqlite3-dev \
      libssl-dev \
      libxml2-dev \
      libxslt1-dev \
      libyaml-dev \
      nodejs \
      npm \
      python-dev \
      python-software-properties \
      sqlite3 \
      vim \
      wget \
      zlib1g-dev

RUN pip install awsebcli

RUN wget -c http://ftp.ruby-lang.org/pub/ruby/2.2/ruby-2.2.2.tar.gz && \
    tar -xzf ruby-2.2.2.tar.gz && \
    cd ruby-2.2.2 && \
    ./configure && \
    make && \
    make install && \
    export PATH="/usr/local/ruby/bin:$PATH" && \
    echo "gem: --no-ri --no-rdoc" > ~/.gemrc && \
    gem update --system && \
    gem install bundler

RUN npm install -g bower gulp

ENTRYPOINT bash
